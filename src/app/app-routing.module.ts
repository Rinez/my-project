import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './home/admin/admin.component';
import { CustomerComponent } from './home/customer/customer.component';

const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginComponent
  },
  {
    path: 'admin',
    pathMatch: 'full',
    component: AdminComponent
  },
  {
    path: 'wheels',
    pathMatch: 'full',
    component: CustomerComponent
  },

  {
    path: '',
    pathMatch: 'full',
    component: LoginComponent,

  },
  {
    path: '**',
    redirectTo: ''
  },
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
